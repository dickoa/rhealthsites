#' Function to clean list from NULL values
#' @param x the list to filter
#' @return a filtered list without NULL elements
#' @noRd
nc <- function(x) Filter(Negate(is.null), x)

#' Bind data and NA to missing columns
#'
#' @param l a list of health facilities
#' @return
#' @noRd
bind_facilities <- function(l = list(...)) {
  cnames <- unique(unlist(lapply(l, names)))
  do.call(rbind, lapply(l, function(x) {
    for(n in cnames[!cnames %in% names(x)]) {
      x[[n]] <- NA
    }
    x
  })
  )
}


#' Set the healthsites API key
#'
#' @param api_key Character the API KEY
#' @return None
#' @export
hs_set_api_key <- function(api_key = NULL) {
  if (!is.null(api_key))
    Sys.setenv("HEALTHSITES_API_KEY" = api_key)
}

#' Check if the Healthsites API key is available
#'
#' @param api_key Character the API KEY
#' @return the api_key when non NULL
#' @export
hs_assert_api_key <- function(api_key)
  if (is.null(api_key) || api_key == "")
    stop("You need to setup the API key using `hs_set_api_key()` function", call. = FALSE)

#' Get the stored Healthsites API key
#'
#' @return A character the API key
#' @export
hs_get_api_key <- function()
  Sys.getenv("HEALTHSITES_API_KEY")

#' Build urls to be used with `crul::Async`
#'
#' @param api_key Healthsites API key
#' @param country Character name of the country of interest
#' @param page Integer the page you want to access
#' @param extent extent of the areas as computed using `hs_format_extent` or character vector in the following format "xmin, ymin, xmax, ymax"
#' @return a Character vector of url with query parameters
#' @noRd
build_urls <- function(api_key = NULL, limit = NULL, country = NULL, page = NULL, extent = NULL)
  paste0("https://healthsites.io/api/v2/facilities/?api-key=", api_key,
         "&page=", page,
         "&limit=", limit,
         "&country=", country,
         "&extent=", extent,
         "&flat-properties=TRUE&output=geojson")

#' Healthsites API Client
#'
#' @importFrom crul HttpClient
#' @return A R6 Crul HttpClient object
#' @noRd
hs_api_client <- function()
  crul::HttpClient$new(url = "https://healthsites.io/")

#' Create an extent object that can be read by the API from sf object or a vector
#'
#' Create a formatted extent object that can fed into the Healthsites API
#' You can use a `sf` object or a numeric vector organized as follow (xmin, ymin, xmax, ymax)
#'
#' @param extent a sf bbox object or a sf object
#' @return an extent collapsed into a character object
#' @noRd
hs_format_extent <- function(extent) {
  if (!is.null(extent)) {
    if (is.vector(extent))
      hs_assert_extent(extent)
    extent <- sf::st_bbox(extent)
    extent <- paste(extent, collapse = ",")
  }
  extent
}


#' @importFrom stats na.omit
#' @noRd
hs_assert_country <- function(x) {
  country_names <- iconv(tolower(country_names), to = "ASCII//TRANSLIT")
  x <- iconv(tolower(x), to = "ASCII//TRANSLIT")
  if (!x %in% country_names) {
    candidate <- agrep(x, country_names, max.distance = 1, ignore.case = TRUE, value = TRUE)
    stop(paste0("Country not available or wrong spelling! Try this spelling: '",
                candidate, "'"), call. = FALSE)
  }
}

#' @noRd
hs_assert_extent <- function(x) {
  if (!is.numeric(x))
    stop("Your extent should be numeric vector!", call. = FALSE)

  if (length(x) != 4)
    stop("Your extent should be a numeric vector of length 4!", call. = FALSE)

  nm <- names(x)
  if (is.null(nm) || !all(nm %in% c("xmin", "ymin", "xmax", "ymax")))
    stop("Your extent should be a named vector with 'xmin', 'ymin', 'xmax' and 'ymax' values!", call. = FALSE)

  if (any(abs(x) > 180))
    stop("Your extent exceed the allowed bound!", call. = FALSE)

  if (!(x[1] <= x[3] && x[2] <= x[4]))
    stop("Your extent should be in the following format c(xmin, ymin, xmax, ymax)", call. = FALSE)
}
