## Test environments

* ubuntu 12.04 (on gitlab-ci), R 3.3.2
* win-builder (devel and release)

## R CMD check results

* Note about license:
License components with restrictions and base license permitting such:
  MIT + file LICENSE
File 'LICENSE':
  YEAR: 2017
  COPYRIGHT HOLDER: Ahmadou Dicko
  
## Reverse dependencies
